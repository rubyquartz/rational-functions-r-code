---
title: "Rational Functions"
author: "Crista Moreno"
date: "November 8, 2016"
output:
  html_document: default
  pdf_document: default
---

<!-- ```{r, out.width = "50px", out.height="50px", echo=FALSE} -->
<!-- knitr::include_graphics("cc_large.png") -->
<!-- knitr::include_graphics("cc_by_large.png") -->
<!-- knitr::include_graphics("sa_large.png") -->
<!-- ``` -->

## Description

-------------------

*Rational Functions* displays the graphs and R code for plotting rational functions and their asymptotes.

<span style="color:magenta">This RNotebook was created using the **Free Software** R and Rstudio. 
**Free software** is vital in protecting the freedoms of users and creators. </span>

Copyright (C) 2016 Crista Moreno

    This program is free software: you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


## Definition of a Rational Function 

---------------------------------------

A **rational function** is a function $f(x) = \dfrac{p(x)}{q(x)}$ where $p(x)$ and $q(x)$ are polynomials. The function $f(x)$ is defined for all real values $\mathbb{R}$ except where $q(x) = 0$.

#### Graphical constants 

--------------------------

```{r}
# line width grid 
lwg <- 1.5

# line width curve
lwc <- 8
```


### Example 1

----------------

$f(x) = \dfrac{1}{x + 1}$


```{r, fig.width = 6, fig.height=5}
# rational function
f <- function(x) {1/(1*x + 1)}

# domain
x <- c(-10:10)
y <- f(x)

min_y <- f(min(x))
max_y <- f(max(x))

color = "blue"

plot(x, f(x), col="hotpink", cex=1, pch=19, cex.lab = 2, cex.axis = 1.3, xlab="x", 
     ylab="y", main="f(x) = 1/(x + 1)", cex.main=2, col.main=color) 

# add grid 
abline(v=(seq(min(x) ,max(x),1)), col="lightgray", lty="dotted", lwd = lwg)
abline(h=(seq(-1,1, 0.1)), col="lightgray", lty="dotted", lwd=lwg)

# add graph of function to plotted points
curve(f, min(x), max(x), n=100, col=color, lwd=lwc, add=TRUE)

# vertical asymptote
abline(v=-1, col="green", lty="dotted", lwd = 7)
```

<!-- ```{r} -->
<!-- cairo_ps(file = "rational_function_example_1.eps", onefile = FALSE, fallback_resolution = 600) -->

<!-- dev.off() -->
<!-- ``` -->

#### Example 2 

----------------

<!-- cairo_ps(file = "rational_function_example_2.eps", onefile = FALSE, fallback_resolution = 600) -->
<!-- dev.off() -->

$f(x) = \dfrac{x - 4}{3x + 2}$

```{r, fig.width = 6, fig.height=5}
# rational function
f <- function(x) {(1*x - 4)/(3*x + 2)}

# domain
x <- c(-5:5)
y <- f(x)

min_y <- f(min(x))
max_y <- f(max(x))

color = "deeppink"

plot(x, f(x), col="hotpink", cex=1, pch=19, cex.lab = 2, cex.axis = 1.3, xlab="x", 
     ylab="y", main="f(x) = (x - 4)/(3x + 2)", cex.main=2, col.main=color) 

# add grid 
abline(v=(seq(min(x) ,max(x),1)), col="lightgray", lty="dotted", lwd = lwg)
abline(h=(seq(-2,5, 1)), col="lightgray", lty="dotted", lwd=lwg)

# add graph of function to plotted points
curve(f, min(x), max(x), n=100, col=color, lwd=lwc, add=TRUE)

# vertical asymptote
abline(v=-2/3, col="yellow", lty="dotted", lwd = 7)
```

There is a vertical asymptote at $x = \dfrac{-2}{3}$, since that is where the denominator is equal to zero.

#### Example 3

----------------

$f(x) = \dfrac{5}{(x^{2} - 3x)}$

```{r, fig.width = 6, fig.height=5}
f <- function(x) {5/(x^{2} -3*x)}

# domain
x <- c(-6:6)
y <- f(x)

min_y <- f(min(x))
max_y <- f(max(x))

color = "springgreen3"
plot(x, f(x), col="hotpink", cex=1, pch=19, cex.lab = 2, cex.axis = 1.3, xlab="x", 
     ylab="y", main="f(x) = 5/(x^{2} - 3x)", cex.main=2, col.main=color) 

# add grid 
abline(v=(seq(min(x) ,max(x),1)), col="lightgray", lty="dotted", lwd = lwg)

abline(h=(seq(-2,5, 1)), col="lightgray", lty="dotted", lwd=lwg)

# add graph of function to plotted points
curve(f, min(x), max(x), n=100, col=color, lwd=lwc, add=TRUE)

# vertical asymptote
abline(v=c(0, 3), col="yellow", lty="dotted", lwd = 7)
```

Notice that there are two asymptotes at  $x=0$ and $x=3$.

#### Example 4

-----------------

$f(x) = \dfrac{x}{(x + 16)} - 3$

```{r, fig.width = 6, fig.height=5}
# rational function
f <- function(x) {1*x/(x + 16) - 3}

# domain
x <- c(-23:-12)
y <- f(x)

min_y <- f(min(x))
max_y <- f(max(x))

color = "magenta4"

plot(x, f(x), col="hotpink", cex=1, pch=19, cex.lab = 2, cex.axis = 1.3, xlab="x", 
     ylab="y", main="f(x) = x/(x + 16) - 3", cex.main=2, col.main=color) 

# add grid 
abline(v=(seq(min(x) ,max(x),1)), col="lightgray", lty="dotted", lwd = lwg)
abline(h=(seq(-15,15, 5)), col="lightgray", lty="dotted", lwd=lwg)

# add graph of function to plotted points
curve(f, min(x), max(x), n=100, col=color, lwd=lwc, add=TRUE)

# vertical asymptote
abline(v=c(-16, 3), col="olivedrab2", lty="dotted", lwd = 7)
```

#### Example 5

----------------

```{r, fig.width = 6, fig.height=5}
# rational function
f <- function(x) {(2*x + 1)/(3*x - 2)}

# domain
x <- c(-5:5)
y <- f(x)

min_y <- f(min(x))
max_y <- f(max(x))

points <- c(3)
color = "turquoise2"

plot(x, f(x), col="hotpink", cex=1, pch=19, cex.lab = 2, cex.axis = 1.3, xlab="x", 
     ylab="y", main="f(x) = (2x + 1)/(3x - 2)", cex.main=2, col.main=color) 

# add grid 
abline(v=(seq(min(x) ,max(x),1)), col="lightgray", lty="dotted", lwd = lwg)
abline(h=(seq(-0.5,3, 0.5)), col="lightgray", lty="dotted", lwd=lwg)

# add graph of function to plotted points
curve(f, min(x), max(x), n=100, col=color, lwd=lwc, add=TRUE)
# vertical asymptote
abline(v=c(2/3), col="violet", lty="dotted", lwd = 7)

# plot point (3, 1)
points(points, f(points), col= "slateblue1", cex = 3, pch=19)
```

#### Example 6

-----------------

$f(x) = \dfrac{x + 5}{x^{2} + 2x - 15}$

```{r, fig.width = 6, fig.height=5}
# rational function
f <- function(x) {(1*x + 5)/(x^{2} + 2*x - 15)}

# domain
x <- c(-10:10)
y <- f(x)

min_y <- f(min(x))
max_y <- f(max(x))

color = "violet"

plot(x, f(x), col="hotpink", cex=1, pch=19, cex.lab = 2, cex.axis = 1.3, xlab="x", 
     ylab="y", main="f(x) = (x + 5)/(x^{2} + 2x - 15)", cex.main=2, col.main=color) 

# add grid 
abline(v=(seq(min(x) ,max(x),1)), col="lightgray", lty="dotted", lwd = lwg)

abline(h=(seq(-0.5,3, 0.5)), col="lightgray", lty="dotted", lwd=lwg)

# add graph of function to plotted points
curve(f, min(x), max(x), n=100, col=color, lwd=lwc, add=TRUE)

# vertical asymptote
abline(v=c(3), col="cyan", lty="dotted", lwd = 7)
```


There is a vertical asymptote at $x = 3$ since $3^{2} + 2*3 - 15$ is zero. This can be seen easily by factoring the denominator 

$q(x) = x^{2} + 2x - 15 = (x - 3)(x + 5)$. 

Note that this rational function has a whole at $x = -5$, since both the numerator and the denominator have $(x + 5)$ as a factor.

#### Example 7 

----------------

$f(x) = \dfrac{6x^{2} + 7x - 5}{2x^{2} - 11x + 5}$

```{r, fig.width = 6, fig.height=5}
f <- function(x) {(6*x^{2} + 7*x - 5)/(2*x^{2} - 11*x + 5)}

# domain
x <- c(-10:15)
y <- f(x)

min_y <- f(min(x))
max_y <- f(max(x))

color = "darkviolet"

plot(x, f(x), col="hotpink", cex=0.5, pch=19, cex.lab = 2, cex.axis = 1.3, xlab="x", 
     ylab="y", main="f(x) = (6x^{2} + 7x -5)/(2x^{2} - 11x + 5)", cex.main=2, col.main=color) 

# add grid 
abline(v=(seq(min(x) ,max(x),1)), col="lightgray", lty="dotted", lwd = lwg)
abline(h=(seq(-25,25, 5)), col="lightgray", lty="dotted", lwd=lwg)

# add graph of function to plotted points
curve(f, min(x), max(x), n=100, col=color, lwd=lwc, add=TRUE)

# vertical asymptote
abline(v=c(5), col="hotpink", lty="dotted", lwd = 7)
```

Notice that there is a vertical asymptote at $x = 5$.

$\dfrac{6x^{2} + 7x - 5}{2x^{2} - 11x + 5} = \dfrac{6x^{2} + 7x - 5}{(2x - 1)(x - 5)}$

#### Example 8 

-----------------

$\dfrac{(x^{3} - x)}{x^{4}(x^2 - 9)}$

```{r, fig.width = 6, fig.height=5}
# rational function
f <- function(x) {((1*x^{3} - 1*x)) /((1*x^{4})*(1*x^{2} - 9)) }

# domain
x <- c(-3.5:3.5)
y <- f(x)

color = "violetred1"

plot(x, f(x), col="hotpink", cex=0.5, pch=19, cex.lab = 2, cex.axis = 1.3, xlab="x", 
     ylab="y", main="f(x) = (x^3 - x)/(x^4(x^2 - 9))", cex.main=2, col.main=color) 

# add grid 
abline(v=(seq(floor(min(x)) ,floor(max(x)),1)), col="lightgray", lty="dotted", lwd = lwg)
abline(h=(seq(-1,1, 0.2)), col="lightgray", lty="dotted", lwd=lwg)

# add graph of function to plotted points
curve(f, min(x), max(x), n=100, col=color, lwd=7, add=TRUE)

# vertical asymptote
abline(v=c(-3, 3, 0), col="seagreen1", lty="dotted", lwd = 7)

points <- c(1, -1)
# plot point (3, 1)
points(points, f(points), col= "navyblue", cex = 2, pch=19)
```

$\dfrac{(x^{3} - x)}{x^{4}(x^2 - 9)}$

$= \dfrac{x(x^{2} - 1)}{x^{4}(x + 3)(x - 3)}$

$= \dfrac{x(x + 1)(x - 1)}{x^{4}(x + 3)(x - 3)}$

$= \dfrac{(x + 1)(x - 1)}{x^{3}(x + 3)(x - 3)}$

Notice that the function is undefined for $x=3,-3$ and $x=0$.